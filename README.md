Objectives and Key Results Doctypes Add-on
==========================================

## Overview

This is a free add-on for [projectdoc](https://www.smartics.eu/confluence/display/PDAC1/) for Confluence.

The add-on provides the blueprints to create pages for

  * Objectives
  * Objective Types
  * Key Results
  * Key Result Types
  * Strategic Themes
  * Strategic Theme Types
  * Check-Ins
  * Check-In Types
  * Wrap-ups
  * Wrap-up Types

It also provides space blueprints to get started with your documentation project quickly.

## Fork me!
Feel free to fork this project to adjust the templates according to your project requirements.

The projectdoc Objectives and Key Results Doctypes Add-on is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Documentation

For more information please visit

  * the [add-on's homepage](https://www.smartics.eu/confluence/x/YIK1BQ)
  * Coming soon: the [add-on on the Atlassian Marketplace](https://marketplace.atlassian.com/plugins/de.smartics.atlassian.confluence.smartics-doctype-addon-okrs)
