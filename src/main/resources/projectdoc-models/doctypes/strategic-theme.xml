<?xml version='1.0'?>
<!--

    Copyright 2018-2024 Kronseder & Reiner GmbH, smartics

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

-->
<doctype
  xmlns="http://smartics.de/xsd/projectdoc/doctype/1"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  id="strategic-theme"
  base-template="standard"
  provide-type="standard-type"
  context-provider="de.smartics.projectdoc.okrs.blueprints.StrategicThemeContextProvider">
  <resource-bundle>
    <l10n>
      <name>Strategic Theme</name>
      <description>
        Document an OKR strategic theme.
      </description>
      <about>
        Publish a strategic theme to group objectives and to share them with
        your colleagues. Note that each objective should be associated with
        exactly one them. If an objective supports two or more themes, try to
        break up the objective in smaller parts.
      </about>
    </l10n>
    <l10n locale="de">
      <name plural="Stategische Leitmotive">Strategisches Leitmotiv</name>
      <description>
        Dokumentieren Sie eines Ihrer strategischen OKR-Leitmotive.
      </description>
      <about>
        Publizieren Sie eines Ihrer strategischen Leitmotive, um sie mit
        Kollegen zu teilen. Beachten Sie, dass Sie Zielvereinbarungen mit
        exakt einem Leitmotiv verbinden sollten. Scheint eine Zielvereinbarung
        mehrere Motive zu unterstützen, versuchen Sie die komplexe
        Zielvereinbarung zu zerlegen.
      </about>
      <type plural="Typen von stategischen Leitmotiven">Typ eines strategischen Leitmotivs</type>
    </l10n>
  </resource-bundle>

  <properties>
    <property key="projectdoc.doctype.common.name">
      <value><xml><![CDATA[<at:var at:name="projectdoc_doctype_common_name"/>]]></xml></value>
      <controls>hide</controls>
    </property>
    <property key="projectdoc.doctype.common.parent">
      <value>
        <macro name="projectdoc-transclusion-parent-property">
          <param name="property-name" key="projectdoc.doctype.common.name" />
          <param name="parent-doctype">strategic-theme</param>
          <param name="property" key="projectdoc.doctype.common.parent" />
        </macro>
      </value>
      <controls>hide</controls>
    </property>
    <property key="projectdoc.doctype.common.type">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">strategic-theme-type</param>
          <param
            name="property"
            key="projectdoc.doctype.common.type" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.strategic-theme.supports">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">strategy,impact</param>
          <param
            name="property"
            key="projectdoc.doctype.strategic-theme.supports" />
          <param name="render-no-hits-as-blank">true</param>
          <param name="render-list-as-comma-separated-values">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Supports</name>
          <description>
            A theme supports a superordinate strategy or desired impact for the
            defined period of time.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Unterstützt</name>
          <description>
            Ein Leitmotiv unterstützt eine übergeordnete Strategie oder Wirkung
            für eine definierte Zeit.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.strategic-theme.owner">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">stakeholder</param>
          <param
            name="property"
            key="projectdoc.doctype.strategic-theme.owner" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
      <resource-bundle>
        <l10n>
          <name>Owner</name>
        </l10n>
        <l10n locale="de">
          <name>Ansprechpartner</name>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.duration.from">
      <value><xml><![CDATA[<at:var at:name="projectdoc.strategic-theme.duration.from" at:rawxhtml="true"/>]]></xml></value>
      <controls>hide</controls>
      <resource-bundle>
        <l10n>
          <description>
            Log the start date of the cycle this strategic theme is pursued.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Halten Sie den Startpunkt für den Zeitabschnitt fest, in dem dieses
            Leitmotiv strategisch verfolgt wird.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.common.duration.to">
      <value><xml><![CDATA[<at:var at:name="projectdoc.strategic-theme.duration.to" at:rawxhtml="true"/>]]></xml></value>
      <controls>hide</controls>
      <resource-bundle>
        <l10n>
          <description>
            Log the end date of the cycle this strategic theme is pursued.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Halten Sie den Endpunkt für den Zeitabschnitt fest, in dem dieses
            Leitmotiv strategisch verfolgt wird.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.strategic-theme.cycle">
      <value>
        <xml><![CDATA[<ac:structured-macro ac:name="projectdoc-transclusion-property-display">
      <ac:parameter ac:name="property-name"><at:i18n at:key="projectdoc.doctype.common.duration.from"/></ac:parameter>
    </ac:structured-macro>
    <ac:structured-macro ac:name="projectdoc-transclusion-property-display">
      <ac:parameter ac:name="append-text"> - </ac:parameter>
      <ac:parameter ac:name="property-name"><at:i18n at:key="projectdoc.doctype.common.duration.to"/></ac:parameter>
    </ac:structured-macro>]]></xml>
      </value>
      <resource-bundle>
        <l10n>
          <name>Cycle</name>
          <description>
            Log the period of the OKR cycle this strategic theme is pursued.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Zyklus</name>
          <description>
            Halten Sie den Zeitabschnitt fest, in dem dieses Leitmotiv
            strategisch verfolgt wird.
          </description>
        </l10n>
      </resource-bundle>
    </property>
    <property key="projectdoc.doctype.okr-priority.priority">
      <value>
        <macro name="projectdoc-name-list">
          <param name="doctype">okr-priority</param>
          <param
            name="property"
            key="projectdoc.doctype.okr-priority.priority" />
          <param name="render-no-hits-as-blank">true</param>
        </macro>
      </value>
    </property>
    <property key="projectdoc.doctype.strategic-theme.score">
      <resource-bundle>
        <l10n>
          <name>Score</name>
          <description>
            Log the value scored by the end of the cycle.
            The format of the value is a decimal value between .01 (or .05) and
            1.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Wert</name>
          <description>
            Halten Sie den erreichten Wert am Ende des Zyklus fest.
            Das Format des Wertes ist eine Dezimalzahl zwischen .01 (oder .05)
            und 1.
          </description>
        </l10n>
      </resource-bundle>
    </property>
  </properties>

  <sections>
    <section key="projectdoc.doctype.common.description">
      <config>
        <param name="show-title">false</param>
      </config>
      <resource-bundle>
        <l10n>
          <description>
            In case the short description is not enough, the description section
            provides room for more detailed information about the strategic
            theme.
          </description>
        </l10n>
        <l10n locale="de">
          <description>
            Sollte die Kurzbeschreibung nicht genügen, das strategische
            Leitmotiv ausreichend zu beschreiben, bietet der
            Beschreibungsabschnitt mehr Raum, um detaillierte Informationen
            bereit zu stellen.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.strategic-theme.objectives">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.strategic-theme.objectives"/></ac:parameter>
        <ac:parameter ac:name="css">projectdoc-compact-buttons</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:rich-text-body>
              <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-okrs:projectdoc-blueprint-doctype-objective</ac:parameter>
                <ac:parameter ac:name="buttonLabel"><at:i18n at:key="projectdoc.doctype.objective.create.label"/></ac:parameter>
                <ac:parameter ac:name="inline">true</ac:parameter>
              </ac:structured-macro>
            </ac:rich-text-body>
          </ac:structured-macro>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">objective</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="projectdoc.doctype.objective.score"/>|, <at:i18n at:key="projectdoc.doctype.objective.key-results"/></ac:parameter>
            <ac:parameter ac:name="where">$&lt;AncestorNames&gt;=[${<at:i18n at:key="projectdoc.doctype.common.name"/>}]</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-mode">definition</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">false</ac:parameter>
            <ac:parameter ac:name="render-classes">objectives-table, display-table, objectives</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Objectives</name>
          <description>
            List the objectives associated with this theme.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Zielvereinbarungen</name>
          <description>
            Listen Sie die Zielvereinbarungen, die mit diesem Leitmotive assoziiert
            sind.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.strategic-theme.wrap-ups">
      <xml><![CDATA[      <ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.strategic-theme.wrap-ups"/></ac:parameter>
        <ac:parameter ac:name="css">projectdoc-compact-buttons</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:rich-text-body>
              <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-okrs:projectdoc-blueprint-doctype-okr-wrap-up</ac:parameter>
                <ac:parameter ac:name="buttonLabel"><at:i18n at:key="projectdoc.doctype.okr-wrap-up.create.label"/></ac:parameter>
                <ac:parameter ac:name="inline">true</ac:parameter>
              </ac:structured-macro>
            </ac:rich-text-body>
          </ac:structured-macro>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">okr-wrap-up</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="projectdoc.doctype.okr-wrap-up.date"/>|, <at:i18n at:key="projectdoc.doctype.okr-wrap-up.scoredValue"/>|</ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.okr-wrap-up.date"/>§</ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="render-mode">definition</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-classes">wrap-ups-table, display-table, wrap-ups</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Wrap-ups</name>
          <description>
            Provide a wrap-up for each interval within a cycle.
          </description>
        </l10n>
        <l10n locale="de">
          <name>Wrap-ups</name>
          <description>
            Geben Sie das Ergebnis eines Wrap-ups für jedes Intervall
            innerhalb eines Zyklus an.
          </description>
        </l10n>
      </resource-bundle>
    </section>

    <section key="projectdoc.doctype.strategic-theme.children">
      <xml><![CDATA[<ac:structured-macro ac:name="projectdoc-section">
        <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.strategic-theme.children"/></ac:parameter>
        <ac:parameter ac:name="css">projectdoc-compact-buttons</ac:parameter>
        <ac:rich-text-body>
          <ac:structured-macro ac:name="projectdoc-hide-from-reader-macro">
            <ac:rich-text-body>
              <ac:structured-macro ac:name="create-from-template">
                <ac:parameter ac:name="blueprintModuleCompleteKey">de.smartics.atlassian.confluence.smartics-doctype-addon-okrs:projectdoc-blueprint-doctype-strategic-theme</ac:parameter>
                <ac:parameter ac:name="buttonLabel"><at:i18n at:key="projectdoc.doctype.strategic-theme.create.label"/></ac:parameter>
                <ac:parameter ac:name="inline">true</ac:parameter>
              </ac:structured-macro>
            </ac:rich-text-body>
          </ac:structured-macro>
          <ac:structured-macro ac:name="projectdoc-display-table">
            <ac:parameter ac:name="doctype">strategic-theme</ac:parameter>
            <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/></ac:parameter>
            <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.sortKey"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
            <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
            <ac:parameter ac:name="restrict-to-immediate-children">true</ac:parameter>
            <ac:parameter ac:name="render-mode">definition</ac:parameter>
            <ac:parameter ac:name="render-classes">children-table, display-table, children</ac:parameter>
          </ac:structured-macro>
        </ac:rich-text-body>
      </ac:structured-macro>]]></xml>
    </section>

    <section key="projectdoc.doctype.strategic-theme.supporting-themes">
      <xml><![CDATA[<ac:structured-macro ac:name="projectdoc-section">
    <ac:parameter ac:name="title"><at:i18n at:key="projectdoc.doctype.strategic-theme.supporting-themes"/></ac:parameter>
    <ac:parameter ac:name="intro-text"><at:i18n at:key="projectdoc.doctype.strategic-theme.supporting-themes.introText"/></ac:parameter>
    <ac:rich-text-body>
      <ac:structured-macro ac:name="projectdoc-display-table">
        <ac:parameter ac:name="doctype">strategic-theme</ac:parameter>
        <ac:parameter ac:name="select"><at:i18n at:key="projectdoc.doctype.common.name"/>, <at:i18n at:key="projectdoc.doctype.common.iteration"/>|, <at:i18n at:key="projectdoc.doctype.common.type"/>|, <at:i18n at:key="projectdoc.doctype.common.shortDescription"/>, <at:i18n at:key="projectdoc.doctype.common.spaceKey"/></ac:parameter>
        <ac:parameter ac:name="sort-by"><at:i18n at:key="projectdoc.doctype.common.type"/>, <at:i18n at:key="projectdoc.doctype.common.name"/></ac:parameter>
        <ac:parameter ac:name="space-keys">@all</ac:parameter>
        <ac:parameter ac:name="where">$&lt;<at:i18n at:key="projectdoc.doctype.common.parent"/>&gt;=[${<at:i18n at:key="projectdoc.doctype.common.name"/>}] AND $&lt;<at:i18n at:key="projectdoc.doctype.strategic-theme.cycle"/>&gt;=[${<at:i18n at:key="projectdoc.doctype.strategic-theme.cycle"/>}] NOT $&lt;<at:i18n at:key="projectdoc.doctype.common.spaceKey"/>&gt;=[${<at:i18n at:key="projectdoc.doctype.common.spaceKey"/>}]</ac:parameter>
        <ac:parameter ac:name="render-no-hits-as-blank">true</ac:parameter>
        <ac:parameter ac:name="render-classes">supporting-themes-table, display-table, supporting-themes</ac:parameter>
      </ac:structured-macro>
    </ac:rich-text-body>
  </ac:structured-macro>]]></xml>
      <resource-bundle>
        <l10n>
          <name>Supporting Themes</name>
          <description>
            List all strategic themes that support this theme.
            All themes that declare this theme as their logical parent are
            automatically listed.
          </description>
          <label key="projectdoc.doctype.strategic-theme.supporting-themes.introText">List of supporting themes that are on the same cycle.</label>
        </l10n>
        <l10n locale="de">
          <name>Unterstützende Leitmotive</name>
          <description>
            Listen Sie alle Leitmotive auf, die dieses Leitmotive unterstützen.
            Die Leitmotive, die dieses Leitmotiv als Elterndokument eingetragen
            haben, werden automatisch gelistet.
          </description>
          <label key="projectdoc.doctype.strategic-theme.supportingThemes.intro-text">Liste unterstützender Themen, die auf demselben Zyklus sind.</label>
        </l10n>
      </resource-bundle>
    </section>
  </sections>

  <related-doctypes>
    <doctype-ref id="objective" />
    <doctype-ref id="key-result" />
    <doctype-ref id="okr-wrap-up" />
  </related-doctypes>

  <wizard template="minimal" form-id="strategic-theme-form">
    <field template="name" key="projectdoc.strategic-theme.name"/>
    <field template="short-description"/>
    <field template="text-field" name="projectdoc.strategic-theme.duration.from"/>
    <field template="text-field" name="projectdoc.strategic-theme.duration.to"/>
  </wizard>
</doctype>
