Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-doctype-addon-okrs:create-doctype-template-okr-wrap-up', function(wizard) {
    wizard.on('pre-render.page1Id', function(e, state) {
        state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
      });

	wizard.on("post-render.page1Id", function(e, state) {
        PROJECTDOC.postrenderWizard(e, state);
    });

    wizard.on('submit.page1Id', function(e, state) {
	      var shortDescription = state.pageData["projectdoc.doctype.common.shortDescription"];
	      if (!shortDescription) {
	          alert(AJS.I18n.getText('projectdoc.doctype.common.shortDescription.wizard.missing'));
	          return false;
	      }
    	PROJECTDOC.adjustToLocation(state);
    });

    wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
});