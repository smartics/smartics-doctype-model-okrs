Confluence.Blueprint.setWizard(
		'de.smartics.atlassian.confluence.smartics-doctype-addon-okrs:create-doctype-template-strategic-theme', function(wizard) {
    wizard.on('pre-render.page1Id', function(e, state) {
        state.wizardData.selectedWebItemData = PROJECTDOC.prerenderWizard(e, state);
      });

	wizard.on("post-render.page1Id", function(e, state) {
        $('#projectdoc_strategic-theme_duration_from').datePicker({
        	overrideBrowserDefault: true
        });
        $('#projectdoc_strategic-theme_duration_to').datePicker({
        	overrideBrowserDefault: true
        });

        $('#projectdoc_strategic-theme_duration_from').attr('placeholder', AJS.I18n.getText('projectdoc.strategic-theme.duration.from.placeholder')) ;
        $('#projectdoc_strategic-theme_duration_to').attr('placeholder', AJS.I18n.getText('projectdoc.strategic-theme.duration.to.placeholder')) ;

        PROJECTDOC.postrenderWizard(e, state);
    });

    wizard.on('submit.page1Id', function(e, state) {
    	PROJECTDOC.validateStandardForm(e, state);
    });

    wizard.assembleSubmissionObject = PROJECTDOC.assemblePageCreationData;
});