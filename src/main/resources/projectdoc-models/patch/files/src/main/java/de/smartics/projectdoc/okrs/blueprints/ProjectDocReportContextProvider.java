/*
 * Copyright 2014-2018 smartics, Kronseder & Reiner GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.smartics.projectdoc.okrs.blueprints;

import de.smartics.projectdoc.atlassian.confluence.admin.space.SpaceAdminConsole;
import de.smartics.projectdoc.atlassian.confluence.api.doctypes.Doctype;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderHelper;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.user.User;

import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * Provides information for a report including a name.
 */
public class ProjectDocReportContextProvider
    extends de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocSubdocumentContextProvider {
  // ********************************* Fields *********************************

  // --- constants ------------------------------------------------------------

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public ProjectDocReportContextProvider(
      final ContextProviderSupportService support) {
    super(support);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final ContextProviderHelper handler = createHandler(blueprintContext);
    addPropertiesToContext(handler);

    final DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    final Date today = (Date) blueprintContext
        .get(ContextProviderHelper.CREATION_DATE_INSTANCE);
    final String templateLabel = (String) blueprintContext.get("templateLabel");
    final String formattedDate = format.format(today);
    final I18NBean i18n = user.createI18n();
    final String nameLabel = i18n.getText("projectdoc.doctype." + templateLabel + ".title.prefix",
        new Object[] {formattedDate});

    blueprintContext.put(Doctype.NAME, nameLabel);

    super.updateBlueprintContext(blueprintContext);
    return blueprintContext;
  }

  // --- object basics --------------------------------------------------------

}
