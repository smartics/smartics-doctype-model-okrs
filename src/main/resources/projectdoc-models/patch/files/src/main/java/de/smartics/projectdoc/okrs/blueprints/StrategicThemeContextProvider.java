package de.smartics.projectdoc.okrs.blueprints;

import de.smartics.projectdoc.atlassian.confluence.api.doctypes.Doctype;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ContextProviderSupportService;
import de.smartics.projectdoc.atlassian.confluence.blueprint.provider.ProjectDocSubdocumentContextProvider;

import com.atlassian.confluence.plugins.createcontent.api.contextproviders.BlueprintContext;
import com.atlassian.confluence.util.i18n.I18NBean;

import org.apache.commons.lang3.StringUtils;

/**
 * Checks to initialize the cycles start and end date.
 */
public class StrategicThemeContextProvider
    extends ProjectDocSubdocumentContextProvider {

  // --- constants ------------------------------------------------------------

  private static final String DATE_PICKER_KEY =
      "projectdoc-adjustVarValues-toDatePicker";

  /**
   * The key to the parameter from the wizard to specify the end date of the
   * cycle period.
   * <p>
   * The value of this constant is {@value}.
   * </p>
   */
  private static final String PROPERTY_KEY_FROM =
      "projectdoc.strategic-theme.duration.from";

  /**
   * The key to the parameter from the wizard to specify the start date of the
   * cycle period.
   * <p>
   * The value of this constant is {@value}.
   * </p>
   */
  private static final String PROPERTY_KEY_TO =
      "projectdoc.strategic-theme.duration.to";

  private static final String[] REPLACEMENT_KEYS =
      new String[] {"${PROP}", "${REF}"};

  /**
   * The macro to add if no start or end date is given. Note that there is a
   * placeholder to identify if the from or to property value needs to be
   * replaced.
   */
  private static final String MACRO =
      "<ac:structured-macro ac:name=\"projectdoc-transclusion-property-display-ref\">\n"
          + "                  <ac:parameter ac:name=\"property-name\">${PROP}</ac:parameter>\n"
          + "                  <ac:parameter ac:name=\"referred-property-name\">${REF}</ac:parameter>\n"
          + "                </ac:structured-macro>";

  // ********************************* Fields *********************************

  // --- members --------------------------------------------------------------

  // ****************************** Initializer *******************************

  // ****************************** Constructors ******************************

  public StrategicThemeContextProvider(
      final ContextProviderSupportService support) {
    super(support);
  }

  // ****************************** Inner Classes *****************************

  // ********************************* Methods ********************************

  // --- init -----------------------------------------------------------------

  // --- factory --------------------------------------------------------------

  // --- get&set --------------------------------------------------------------

  // --- business -------------------------------------------------------------

  @Override
  protected BlueprintContext updateBlueprintContext(
      final BlueprintContext blueprintContext) {
    final I18NBean i18n = this.support.getI18nService().createI18n();
    handleCyclePeriodProperty(blueprintContext, i18n,
        PROPERTY_KEY_FROM, "projectdoc.doctype.common.duration.from");
    handleCyclePeriodProperty(blueprintContext, i18n,
        PROPERTY_KEY_TO, "projectdoc.doctype.common.duration.to");

    return super.updateBlueprintContext(blueprintContext);
  }

  private void handleCyclePeriodProperty(
      final BlueprintContext blueprintContext, final I18NBean i18n,
      final String paramKey, final String key) {
    final String paramValue = getAsString(blueprintContext, paramKey);
    if (StringUtils.isBlank(paramValue)) {
      final String propertyName = i18n.getText(Doctype.PARENT);
      final String refName = i18n.getText(key);
      final String macro = StringUtils.replaceEach(MACRO, REPLACEMENT_KEYS,
          new String[] {propertyName, refName});
      blueprintContext.put(paramKey, macro);
    } else {
      //blueprintContext.put(paramKeyWrite, paramValue);
      String pickerValue = getAsString(blueprintContext, DATE_PICKER_KEY);
      if (StringUtils.isBlank(pickerValue)) {
        pickerValue = paramKey;
      } else {
        pickerValue += ',' + paramKey;
      }
      blueprintContext.put(DATE_PICKER_KEY, pickerValue);
    }
  }

  private static String getAsString(final BlueprintContext blueprintContext,
      final String paramKey) {
    final Object value = blueprintContext.get(paramKey);
    if (value instanceof String) {
      return (String) value;
    }
    if (value == null) {
      return null;
    }

    return value.toString();
  }

  // --- object basics --------------------------------------------------------

}
